### script for making periodical check the existence of one project containers
### created by ablaikhan-s

### Create ~/scripts/$project directory
### Copy this script into ~/scripts/$project
### Add crontab JOB like those :
### * * * * * bash ~/scripts/$project/container_existence_checker.sh >> ~/scripts/$project/container_existence_checker.log

date=`date +"%D-%T"`
project="test"
find_by_word="test"
coinainers_count_must_be=3;
enabled_containers_count=`docker ps | grep $find_by_word | wc -l`
disabled_containers_count=`docker ps -a | grep $find_by_word  | grep -i exit | wc -l`
if [[ "$enabled_containers_count" < "$coinainers_count_must_be" && "$disabled_containers_count" > "0" ]] 
then 
        echo "------------------------------------------------"
        echo "$date: ERROR"
        echo "---ALL CONTAINERS-------------------------------"
        echo "------------------------------------------------"
        docker ps -a | grep $find_by_word 
        for container in `docker ps -a | grep $find_by_word  | grep -i exit | awk '{print $NF}'`
        do
                echo "$date: $container" >> ~/scripts/$project/stopped_containers.log
                echo "---$container LOG AND RESTART---------" >> ~/scripts/$project/stopped_containers.log
                echo "------------------------------------------------" >> ~/scripts/$project/stopped_containers.log
                docker logs -t $container tail -n300 >> ~/scripts/$project/stopped_containers.log
                docker start $container
        done
        echo "------------------------------------------------"
elif [[ "$enabled_containers_count" < "$coinainers_count_must_be" && "$disabled_containers_count" = "0" ]]
then 
    echo "------------------------------------------------"
    echo "$date: ERROR"
    echo "Disabled containers were deleted"
    echo "---ALL CONTAINERS-------------------------------"
    echo "------------------------------------------------"
fi