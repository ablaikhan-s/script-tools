### script for changing owner of all tables id database
### created by ablaikhan-s

### make nopasswd sudo perrmission for psql command

dbname="dbname"
username="newuser"
tables_list="./tables_list.txt"

n=0
for table in `cat $tables_list`
do
        n=$(($n+1))
        echo "$n: $table"
        sudo -u postgres psql $dbname -c "ALTER TABLE $table OWNER TO $username "
done