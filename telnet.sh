#!/bin/bash

for node in $(cat MY_SERVERS)
do 
    echo ">>> $node"
    sshpass -p $MY_SERVERS_PASSWORD \
        ssh $node 'echo "open REMOTE_SERVICE_IP REMOTE_SERVICE_PORT" | telnet' 2>/dev/null
done
