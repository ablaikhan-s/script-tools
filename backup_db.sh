### script for making periodical MYSQL/POSTGRES DB backups and store locally or on cloud(yandex s3cmd) 
### created by ablaikhan-s

### make nopasswd sudo perrmission for psql command
### Create $script_path directory
### Copy this script into $script_path
### Install and configure s3cmd
### Add crontab JOB like those :
### 05 * * * * bash $script_path/backup_db.sh hourly
### 00 00 * * * bash $script_path/backup_db.sh daily
### 10 00 28 * * bash $script_path/backup_db.sh monthly

script_path=~/bin/backups 
project="projectname"
time=`date +"%y-%m-%d_%H-%M"`
dumpname="$project.$time.dump"

db_name="database"
db_user=""
db_password=""
container_name=""
bucket="projectname"

periodic=$1
subd="mysql" # "mysql" or "postgres" 
backup_type="cloud" # "cloud" or "local"

# setting backups to keep
if [[ $periodic == "hourly" ]]
then days_to_keep=2
elif [[ $periodic == "daily" ]]
then days_to_keep=30
elif [[ $periodic == "monthly" ]]
then days_to_keep=200
fi 

# notify user 
if [[ $periodic != "hourly" ]] && [[ $periodic != "daily" ]] && [[ $periodic != "monthly" ]]
then
    echo "add paramenter \"hourly\", \"daily\" or \"monthly\" "
    exit 0
fi

# start making backup
cd $script_path
mkdir -p $bucket/$project/$periodic 2>/dev/null || echo "already exists"
echo "$time: start creating dump"
if [[ $subd == "postgres" ]]
# if db is containerized user command:
# docker exec -it -u postgres $container_name sh -c "pg_dump $db_name --insert" > $bucket/$project/$periodic/$dumpname
then sudo -u postgres pg_dump $db_name --insert > $bucket/$project/$periodic/$dumpname
elif [[ $subd == "mysql" ]]
# if db is containerized user command:
# docker exec -it $container_name sh -c "mysqldump -u $db_user -p"$db_password" $db_name" > $bucket/$project/$periodic/$dumpname
then mysqldump -u $db_user -p"$db_password" $db_name > $bucket/$project/$periodic/$dumpname
else 
    echo "set the SUBD vaiable: \"mysql\" or \"postgres\" "
    exit 0;
fi

echo "$time: archiving dump"
tar -zcvf $bucket/$project/$periodic/$dumpname.gz $bucket/$project/$periodic/$dumpname
rm $bucket/$project/$periodic/$dumpname

if [[ "$backup_type" == "cloud" ]]
then 
    # send backup to cloud storage
    echo "$time: sending to cloud storage"
    s3cmd put $bucket/$project/$periodic/$dumpname.gz s3://$bucket/$project/$periodic/ 
    # delete local .gz backup
    rm $bucket/$project/$periodic/$dumpname.gz 
    # delete old dumps on cloud
    backups_count=`s3cmd ls s3://$bucket/$project/$periodic/ | wc -l`  
    if [ "$backups_count" -gt "$days_to_keep" ]                                                                                                                                                                                    
    then                                                                                                                                                                                                                                
        oldest_dump=`s3cmd ls s3://$bucket/$project/$periodic/ | head -n1 | awk '{print $4}'`                                                                                                                                   
        s3cmd rm $oldest_dump                                                                                                                                                                                     
    fi
else 
    # delete old dumps on local
    find $bucket/$project/$periodic/ -mtime +$days_to_keep -name '*.gz' -exec rm -f '{}' \;
fi 


