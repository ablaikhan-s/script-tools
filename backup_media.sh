### script for making periodical media files backups and store locally or on cloud(yandex s3cmd) 
### created by ablaikhan-s

### Create $script_path directory
### Copy this script into $script_path
### Install and configure s3cmd
### Add crontab JOB like those :
### 05 * * * * bash $script_path/backup_media.sh hourly
### 00 00 * * * bash $script_path/backup_media.sh daily

script_path=~/bin/backups 
project="projectname"
time=`date +"%y-%m-%d_%H-%M"`
dumpname="$project.$time.gz"

media_path="path1 path2" 
bucket="bucketname"

periodic=$1
backup_type="cloud" # "cloud" or "local"

if [[ $periodic != "hourly" ]] && [[ $periodic != "daily" ]] && [[ $periodic != "monthly" ]]
then
    echo "add paramenter \"hourly\", \"daily\" or \"monthly\" "
    exit 0
fi

cd $script_path
mkdir -p $bucket/$project/$periodic 2>/dev/null || echo "already exists"
echo "$time: start creating dump"
tar -zcvf $bucket/$project/$periodic/$dumpname $media_path

# for cloud storage
echo "$time: sending to cloud storage"
s3cmd put $bucket/$project/$periodic/$dumpname s3://$bucket/$project/$periodic/ 

# delete old dumps 
if [[ $periodic == "hourly" ]]
then days_to_keep=72
elif [[ $periodic == "daily" ]]
then days_to_keep=30
elif [[ $periodic == "monthly" ]]
then days_to_keep=6
fi 
#echo $days_to_keep

# # delete old dumps on cloud
backups_count=`s3cmd ls s3://$bucket/$project/$periodic/ | wc -l`                                                                                                                                                                       
if [ "$backups_count" -gt "$days_to_keep" ]                                                                                                                                                                                    
then                                                                                                                                                                                                                                
    oldest_dump=`s3cmd ls s3://$bucket/$project/$periodic/ | head -n1 | awk '{print $4}'`                                                                                                                                   
    s3cmd rm $oldest_dump                                                                                                                                                                                       
fi

if [[ "$backup_type" == "cloud" ]]
then 
    # # delete tmp files 
    rm $bucket/$project/$periodic/$dumpname.gz
else 
    # # delete old dumps on local
    find $bucket/$project/$periodic/ -mtime +$days_to_keep -name '*.gz' -exec rm -f '{}' \;
fi